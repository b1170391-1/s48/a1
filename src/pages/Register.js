import {useState, useEffect, useContext} from "react";

import Swal from "sweetalert2";

import {Container, Form, Button} from "react-bootstrap"

import {Redirect} from "react-router-dom";

import UserContext from '../UserContext';

export default function Register() {

	const {user, setUser, Redirect} = useContext(UserContext);
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [cp, setCp] = useState("")
	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [mobileNo, setMobileNo] = useState("")

	const [isDisabled, setIsDisabled] = useState(true)

	function registerUser(e){
		e.preventDefault()

		fetch ("http://localhost:4000/api/users/register", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo,
				email: email,
				password: password,
			})
		})

		.then (res => res.json())
		.then (data => {
			if (data === data.email && data === data.password)
			{
				checkUserExist(data.email)
				Swal.fire({
					icon: "error",
					text: "User Exist"
				})
			}
			else
			{
				Swal.fire({
					title: "User Registration Successful",
					icon: "success",
					text: "Welcome to React Booking System"
				})
			}
		})

		setFirstName("")
		setLastName("")
		setMobileNo("")
		setEmail("")
		setPassword("")
		setCp("")
	}

	useEffect ( () => {
		if (
				(firstName !== "" && 
				lastName !== "" &&
				mobileNo !== "" &&
				email !== "" && 
				password !== "" && 
				cp !== "") 

				&&
				(password === cp)
		  	)
		{
			setIsDisabled(false)
		}
		else 
		{
			setIsDisabled(true)
		}
	}, [firstName, lastName, mobileNo, email, password, cp])

	const checkUserExist = () => {
		fetch("http://localhost:4000/api/users/email-exists", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then (res => res.json())
		.then (data => {
			console.log (data)
			setUser({
				email: data.email,
				password: data.password
			})
		})
	}

	return (

		(user.email === email)?
		<Redirect to = "/login"/>
		:
		<Container>
						<Form 
							className="border p-3"
							onSubmit = {
								(e) => {
										registerUser(e);
									   }
							}
						>

						{/*First Name*/}
							  <Form.Group className="mb-3" controlId="Fname">
							    <Form.Label>First Name</Form.Label>
							    <Form.Control 
							    	type="text" 
							    	placeholder="Enter First Name"
							    	value = {firstName}
							    	onChange={ (e) => {
							    		setFirstName(e.target.value)
							    	}}
							     />
							  </Form.Group>
						{/*Last Name*/}
							  <Form.Group className="mb-3" controlId="Lname">
							    <Form.Label>Last Name</Form.Label>
							    <Form.Control 
							    	type="text" 
							    	placeholder="Enter Last Name"
							    	value = {lastName}
							    	onChange={ (e) => {
							    		setLastName(e.target.value)
							    	}}
							     />
							  </Form.Group>
						{/*Mobile Number*/}
							  <Form.Group className="mb-3" controlId="mobile">
							    <Form.Label>Mobile Number</Form.Label>
							    <Form.Control 
							    	type="text" 
							    	placeholder="Enter Mobile Number"
							    	value = {mobileNo}
							    	onChange={ (e) => {
							    		setMobileNo(e.target.value)
							    	}}
							     />
							  </Form.Group>
						{/*Email*/}
						  <Form.Group className="mb-3" controlId="formBasicEmail">
						    <Form.Label>Email address</Form.Label>
						    <Form.Control 
						    	type="email" 
						    	placeholder="Enter email"
						    	value = {email}
						    	onChange={ (e) => {
						    		setEmail(e.target.value)
						    	}}
						     />
						  </Form.Group>

					  {/*password*/}
					    <Form.Group className="mb-3" controlId="formBasicPassword">
					      <Form.Label>Password</Form.Label>
					      <Form.Control type="password" placeholder="Password"
					      	 value={password}
					      	 onChange = { (e) => {
					      	 	setPassword (e.target.value)
					      	 }}
					      />
					    </Form.Group>

					  {/*Confirm Password*/}

					  <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
					    <Form.Label>Confirm Password</Form.Label>
					    <Form.Control type="password" placeholder="Confirm Password"
					    	  value = {cp}
					    	  onChange = { (e) => {
					    	  	setCp (e.target.value)
					    	  }}

					    />
					  </Form.Group>
					  	{/*Submit*/}
					    <Button 
					    	variant="primary" 
					    	type="submit"
					    	disabled = {isDisabled}
					    >
					      Submit
					    </Button>
					</Form>
				</Container>
	)
}