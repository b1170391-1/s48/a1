import {useState, useEffect, useContext} from "react";
//userContext - unpack the data
import Swal from "sweetalert2";

import {Redirect} from "react-router-dom";

import {
	Container,
	Form,
	Button
} from "react-bootstrap"

import UserContext from '../UserContext';

export default function Login () {

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")

	const [isDisabled, setIsDisabled] = useState(true)

	useEffect ( () => {
		if (email !== "" && password !== "" )
		{
			setIsDisabled(false)
		}
		else 
		{
			setIsDisabled(true)
		}
	}, [email, password])

	function loginUser(e){
		e.preventDefault()

		fetch("http://localhost:4000/api/users/login", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then (res => res.json())
		.then (data=> {

			if (typeof data !== "undefined")
			{
				localStorage.setItem('token', data.access)
				//invoke function to retrieve user details
				userDetails(data.access)

				Swal.fire({
					title: "Login Success",
					icon: "success",
					text: "Welcome"
				})

			} else {
				Swal.fire({
					title: "Login Failed",
					icon: "error",
					text: "Check you Login details, TRY AGAIN"
				})
			}

		})

		setEmail("");
		setPassword("");
	}

	const userDetails = (token) => {
		fetch("http://localhost:4000/api/users/details",{
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then (res => res.json())
		.then (data => {
			console.log (data)

			//use setUser() to update the state
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}


	return (

		(user.id !== null ) ? //if codeblock

			<Redirect from = "/login" to = "/courses"/>
		: //else codeblock
			<Container>
						<Form 
							className="border p-3 mt-5"
							onSubmit = {
								(e) => {
									loginUser(e);
								}
							}
						>
						  <Form.Group className="mb-3" controlId="formBasicEmail">
						    <Form.Label>Email address</Form.Label>
						    <Form.Control 
						    	type="email" 
						    	placeholder="Enter Email" 
						    	value = {email}
						    	onChange={ (e) => {
						    		setEmail(e.target.value)
						    	}}
						    	/>
						  </Form.Group>

						  <Form.Group className="mb-3" controlId="formBasicPassword">
						    <Form.Label>Password</Form.Label>
						    <Form.Control 
						    	type="password" 
						    	placeholder="Password"
						    	value={password}
						    	onChange = { (e) => {
						    	 	setPassword (e.target.value)
						    	 }}

						    />
						  </Form.Group>
						  <Button 
						  	variant="primary" 
						  	type="submit"
						  	disabled = {isDisabled} 
						  >
						    Submit
						  </Button>
						</Form>
					</Container>
	)
}