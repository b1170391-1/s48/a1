import {Fragment, useEffect, useState, useContext} from 'react'
// import courseData from './../data/courseData';

import UserContext from "./../UserContext"
console.log(user)

/*components*/
	/*CourseCard is the template for courses*/
import CourseCard from './../components/CourseCard';

export default function Courses(){

	const [courses, setCourses] = useState([])

	const {user} = useContext(UserContext);

	// console.log(courseData)	//array of objects
	// const courses = courseData.map( element => {
	// 	console.log(element)	//each object in the courseData array
	// 	return(
	// 		<CourseCard key={element.id} courseProp={element}/>
	// 	)
	// })

	const fetchData = () => {
		fetch("http://localhost:4000/api/courses")
		.then (res => res.json())
		.then (data => {
			console.log(data)
			setCourses(data)
		})
	}

	useEffect ( () => {

		fetchData()
		
	}, [])
	console.log (courses);
	return (
		<Fragment>
			{

			}
		</Fragment>
	)
}